extends CanvasLayer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$TextureProgress.max_value = 100
	$TextureProgress.value = 100
	$GameWon.hide()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_player_Update_Health(value):
	$Health_Value.text = str(value)
	$TextureProgress.value = value
	
	pass # Replace with function body.


func _on_player_Player_Won():
	$GameWon.show()
	pass # Replace with function body.
