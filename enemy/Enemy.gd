extends KinematicBody2D

export (int) var Health = 1

var bCanBeEaten = false
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var Velocity = Vector2(0,0)
var Gravity = 1800
var speed = 0;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func AddEnemy(pos, direction):
	position = pos
	if direction < 0:
		speed = speed * -1
	Velocity.x = direction
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Health == 0:
		$AnimatedSprite.play("die")
		bCanBeEaten = true
		Velocity = Vector2(0,0)
		speed = 0
		pass	
	pass


func _physics_process(delta):
	Velocity.x = speed
	Velocity.y = Gravity * delta;
	var collision = move_and_collide(Velocity*delta)
	
	if collision:
		if collision.collider.has_method("PlayerTriesToEatMonster"):
			collision.collider.PlayerTriesToEatMonster(self)
		
	if not $VisibilityNotifier2D.is_on_screen():
		queue_free()
		
	pass

func EnemyHit():
	if Health > 0:
		Health  -= 1;
	pass


func _on_AnimatedSprite_animation_finished():
	bCanBeEaten = true
	pass # Replace with function body.

