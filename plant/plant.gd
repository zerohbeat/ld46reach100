extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal Player_Touches_Plant

# Called when the node enters the scene tree for the first time.
func _ready():
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	pass

func _on_Plant_body_entered(body):
	if body.name == "player" && !$CollisionShape2D.is_disabled():
		emit_signal("Player_Touches_Plant")
		$CollisionShape2D.set_disabled(true)
		hide()
	pass # Replace with function body.

