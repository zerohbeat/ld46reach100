extends Node2D

const PlantInstance = preload("res://plant/plant.tscn")
const EnemyInstance = preload("res://enemy/Enemy.tscn")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():

	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	



func _on_player_Player_Turns_To_Child():
	
	
	
	pass # Replace with function body.


func _on_player_Player_Turns_Child():
	var Enemy = EnemyInstance.instance()
	Enemy.AddEnemy(Vector2(52,510), 0)
	add_child(Enemy)
	
	var Enemy1 = EnemyInstance.instance()
	Enemy1.AddEnemy(Vector2(98,67), 0)
	add_child(Enemy1)
	
	var Enemy2 = EnemyInstance.instance()
	Enemy2.AddEnemy(Vector2(158,864), 0)
	add_child(Enemy2)
	
	var Enemy3 = EnemyInstance.instance()
	Enemy.AddEnemy(Vector2(388,580), -0)
	add_child(Enemy3)
	
	var Enemy4 = EnemyInstance.instance()
	Enemy4.AddEnemy(Vector2(583,858), -0)
	add_child(Enemy4)
	
	var Enemy5 = EnemyInstance.instance()
	Enemy5.AddEnemy(Vector2(956,863), -0)
	add_child(Enemy5)
	
	var Enemy6 = EnemyInstance.instance()
	Enemy6.AddEnemy(Vector2(1036,509), -0)
	add_child(Enemy6)
	
	var Enemy7 = EnemyInstance.instance()
	Enemy7.AddEnemy(Vector2(1434,590), -0)
	add_child(Enemy7)
	
	pass # Replace with function body.


func _on_player_Player_Won():
	$Timer.start()
	pass # Replace with function body.


func _on_Timer_timeout():
	get_tree().change_scene("res://mainmenu/MainMenu.tscn")
	pass # Replace with function body.
