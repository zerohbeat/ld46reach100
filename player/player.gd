extends KinematicBody2D

const MagicInstance = preload("res://bullet/Bullet.tscn")

signal Player_Won

signal Player_Touches_Plant
signal Player_Turns_To_Baby
signal Player_Turns_Child
signal Update_Health

enum CharacterStage {
	Egg,
	EggToBaby,
	Baby,
	BabytoChild,
	Child,
	ChildtoYoungAdult,
	YoungAdult
}

export var m_iCurrentStage =0
export (int) var PlayerSpeed = 100
export (int) var Gravity = 300
export (int) var JumpSpeed = 1000
export (int) var Health = 100
export (int) var NumberofPlantstoChild = 3
var bCanJump = true

var screen_size


var PlayerVelocity = Vector2()

#egg 
export (int) var m_iEggTiltNumber = 0
export (int) var m_iEggTiltMaxNumber = 4

#important when "it" is an egg
var bCanPlayAnimation 

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func BabyLogic():
	if m_iEggTiltNumber == 0 && bCanPlayAnimation:
		if Input.is_action_pressed('ui_left'):
			$player_anim_sprite.play("egg_cracking_0")
			$CrackCrack.play();
			bCanPlayAnimation = false
	if m_iEggTiltNumber == 1 && bCanPlayAnimation:
		if Input.is_action_pressed('ui_right'):
			$player_anim_sprite.play("egg_cracking_1")
			$CrackCrack.play();
			
			bCanPlayAnimation = false
	if m_iEggTiltNumber == 2 && bCanPlayAnimation:
		if Input.is_action_pressed("ui_left"):
			$player_anim_sprite.play("egg_cracking_2")
			$CrackCrack.play();
			
			bCanPlayAnimation = false
	if m_iEggTiltNumber == 3 && bCanPlayAnimation:
		if Input.is_action_pressed('ui_right'):
			$player_anim_sprite.play("egg_cracking_3")
			$CrackCrack.play();
			
			bCanPlayAnimation = false
	pass


func ChildGetInput():
	PlayerVelocity.x = 0
	PlayerVelocity.y = PlayerVelocity.y
	PlayerVelocity.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	
	if is_on_floor():
		bCanJump = true;
	else:
		bCanJump = false;
		
	if Input.is_action_just_pressed("ui_down"):
		var newmagic = MagicInstance.instance()
		newmagic.init_bullet($magiclocation.global_position, rotation)
		get_parent().add_child(newmagic)
		pass
	
	
	if Input.is_action_just_pressed("ui_up") && bCanJump:
		PlayerVelocity.y -= JumpSpeed
		$player_anim_sprite.play("baby_jumping")
	
	if PlayerVelocity.x > 0:
		PlayerVelocity.x += PlayerSpeed
	if PlayerVelocity.x < 0:
		PlayerVelocity.x -= PlayerSpeed
	pass

func BabyGetInput():
	PlayerVelocity.x = 0
	PlayerVelocity.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	if PlayerVelocity.x > 0:
		PlayerVelocity.x += PlayerSpeed
	if PlayerVelocity.x < 0:
		PlayerVelocity.x -= PlayerSpeed
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	#m_iCurrentStage =  CharacterStage.Egg
	$player_anim_sprite.animation = "player_egg"
	m_iEggTiltNumber = 0
	bCanPlayAnimation = true
	emit_signal("Update_Health", self.Health)
	screen_size = get_viewport_rect().size
	pass # Replace with function body.

 #Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Health <= -1:
		get_tree().change_scene("res://gameover/GameOver.tscn")
	
	#baby logic
	if m_iCurrentStage == CharacterStage.Egg:
		BabyLogic()
		pass
	
	if m_iCurrentStage == CharacterStage.Baby && NumberofPlantstoChild == 0:
		m_iCurrentStage = CharacterStage.BabytoChild
		pass
	
	#when eggtobaby
	if m_iCurrentStage == CharacterStage.BabytoChild:
		$player_anim_sprite.play("baby_to_child")	
	
	#when  evolving from egg to baby
	if m_iCurrentStage == CharacterStage.EggToBaby:
		$player_anim_sprite.play("egg_to_baby")
	
	if m_iCurrentStage == CharacterStage.Baby:		
		if PlayerVelocity.x == 0:
			$player_anim_sprite.play("baby_idle")
		else:
			$player_anim_sprite.play("baby_walking")
			if PlayerVelocity.x >= 0:
				$player_anim_sprite.flip_h = false
			else:
				$player_anim_sprite.flip_h = true;
		pass
	
	if m_iCurrentStage == CharacterStage.Child:
		if $player_anim_sprite.animation == "baby_jumping":
			pass
		else:
			if PlayerVelocity.x == 0:
				$player_anim_sprite.play("child_idle")
			else:
				$player_anim_sprite.play("child_walking")
	
	if PlayerVelocity.x >= 0:
		$player_anim_sprite.flip_h = false
	else:
		$player_anim_sprite.flip_h = true;
		pass
	
	
	if Health >= 100:
		Health	= 100
		emit_signal("Player_Won")
	
	
	pass

func _physics_process(delta):
	
	if m_iCurrentStage == CharacterStage.BabytoChild:
		PlayerVelocity = Vector2(0,0)
		pass
	if m_iCurrentStage == CharacterStage.Baby:
		BabyGetInput()
	
	if m_iCurrentStage == CharacterStage.Child:
		ChildGetInput()
	
	PlayerVelocity.y += Gravity * delta;
	PlayerVelocity = move_and_slide(PlayerVelocity, Vector2(0,-1))
	
	
	self.position.x = clamp(position.x,0, screen_size.x)
	self.position.y = clamp(position.y,0, screen_size.y)
	pass


func _on_player_anim_sprite_animation_finished():
	if m_iCurrentStage == CharacterStage.EggToBaby:
		m_iCurrentStage = CharacterStage.Baby
		emit_signal("Player_Turns_To_Baby")	
		pass
	
	if m_iCurrentStage == CharacterStage.BabytoChild:
		m_iCurrentStage = CharacterStage.Child
		PlayerSpeed = 5*PlayerSpeed
		emit_signal("Player_Turns_Child")
		pass
	
	if m_iCurrentStage == CharacterStage.Egg:
		$player_anim_sprite.stop()
		m_iEggTiltNumber+= 1
		bCanPlayAnimation = true
		if m_iEggTiltNumber >= m_iEggTiltMaxNumber:
			m_iCurrentStage = CharacterStage.EggToBaby
	
			
	pass # Replace with function body.


func _on_plant_Player_Touches_Plant():
	self.Health += 10
	if Health >= 100:
		Health	= 100
		emit_signal("Player_Won")
		
	emit_signal("Update_Health", self.Health)
	$BabyEating.play()
	if m_iCurrentStage == CharacterStage.Baby:
		NumberofPlantstoChild -= 1
	pass # Replace with function body.

func _on_Player_Health_Timer_timeout():
	self.Health -= 1
	emit_signal("Update_Health", self.Health)
	pass # Replace with function body.

func PlayerTriesToEatMonster(monster):
	if monster.Health == 0:
		self.Health += 10
		emit_signal("Update_Health", self.Health)
		monster.queue_free()
		$BabyEating.play()
		if Health >= 100:
			Health	= 100
	else:
		self.Health -= 20
		emit_signal("Update_Health", self.Health)	
	pass

