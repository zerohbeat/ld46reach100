extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var speed = 800
var velocity 

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func init_bullet(pos, direction):
	rotation = 0
	position = pos
	velocity = Vector2(0, speed).rotated(rotation)

func _physics_process(delta):
	var collision = move_and_collide(velocity* delta)
	if collision:
		if collision.collider.has_method("EnemyHit"):
			collision.collider.EnemyHit()
		self.queue_free()
	pass



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
